from datetime import date, datetime, timedelta

import pytest

from karpet import Karpet

CRYPTOCOMPARE_API_KEY = None


def get_last_week():
    return date.today() - timedelta(days=13), date.today() - timedelta(days=7)


def test_fetch_crypto_historical_data():
    c = Karpet()

    assert 1000 < len(c.fetch_crypto_historical_data("btc", slug="bitcoin"))


def test_fetch_crypto_historical_data_params():
    c = Karpet()

    with pytest.raises(TypeError):
        c.fetch_crypto_historical_data()


def test_fetch_crypto_historical_data_limited():
    c = Karpet(date(2019, 1, 1), date(2019, 1, 30))

    assert 30 == len(c.fetch_crypto_historical_data("btc", slug="bitcoin"))


def test_fetch_exchanges():
    c = Karpet()
    exchanges = c.fetch_crypto_exchanges("btc")

    assert len(exchanges) > 0
    assert "Binance" in exchanges


# def test_fetch_google_trends():
#     c = Karpet(*get_last_week())
#     df = c.fetch_google_trends(kw_list=["bitcoin"])
#
#     assert len(df) > 0
#     assert df["bitcoin"].max() <= 100
#     assert df["bitcoin"].min() >= 1


def test_fetch_news():
    k = Karpet()
    news = k.fetch_news("eth")

    assert len(news) > 0
    assert "url" in news[0]
    assert "title" in news[0]
    assert "date" in news[0]

    if news[0]["date"] is not None:
        assert isinstance(news[0]["date"], datetime)


def test_fetch_news_with_limit():
    k = Karpet()
    news = k.fetch_news("eth", limit=30)

    assert 0 < len(news) <= 30
    print(f"Fetched {len(news)} news.")


def test_get_basic_info():
    k = Karpet()
    data = k.get_basic_info(slug="ethereum")

    assert isinstance(data["name"], str)
    assert isinstance(data["current_price"], float)
    assert isinstance(data["market_cap"], int)
    assert isinstance(data["rank"], int)
    assert isinstance(data["reddit_average_posts_48h"], float)
    assert isinstance(data["reddit_average_comments_48h"], float)
    assert isinstance(data["reddit_subscribers"], int)
    assert isinstance(data["reddit_accounts_active_48h"], float)
    assert isinstance(data["forks"], int)
    assert isinstance(data["stars"], int)
    assert isinstance(data["total_issues"], int)
    assert isinstance(data["closed_issues"], int)
    assert isinstance(data["open_issues"], int)
    assert isinstance(data["pull_request_contributors"], int)
    assert isinstance(data["commit_count_4_weeks"], int)
    assert isinstance(data["year_low"], float)
    assert isinstance(data["year_high"], float)
    assert isinstance(data["yoy_change"], float)


def test_get_coin_slugs():
    k = Karpet()
    assert k.get_coin_slugs("BTC") == ["bitcoin"]


def test_fetch_crypto_live_data():
    k = Karpet()
    df = k.fetch_crypto_live_data(slug="ethereum")

    assert 0 < len(df)
    assert list(df.columns) == ["open", "high", "low", "close"]


def test_quick_search_data():
    k = Karpet()

    for ident in ["bitcoin", "ethereum", "trx"]:
        item = k.get_quick_search_data(ident)[0]

        assert "thumb" in item
        assert "name" in item
        assert "symbol" in item
        assert "market_cap_rank" in item
        assert "id" in item
        assert "coin_id" in item
        assert "data" in item
        assert "price" in item["data"]
        assert "price_btc" in item["data"]
        assert "price_change_percentage_24h" in item["data"]
        assert "market_cap" in item["data"]
        assert "market_cap_btc" in item["data"]
        assert "total_volume" in item["data"]
        assert "total_volume_btc" in item["data"]
        assert "sparkline" in item["data"]
        assert "content" in item["data"]
        assert "title" in item["data"]["content"]
        assert "description" in item["data"]["content"]
